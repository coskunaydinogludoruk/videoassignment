import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './authentication/constants';
import { JwtStrategy } from './authentication/jwt.strategy';
import { UsersService } from './users/users.service';
import { userProviders } from './users/user.providers';
import { DatabaseModule } from './database/database.module';
import { AuthenticationController } from './authentication/authentication.controller';
import { AuthenticationService } from './authentication/authentication.service';
import { VideosModule } from './videos/videos.module';
import { videoProviders } from './videos/video.providers';
import { BullModule } from '@nestjs/bull';
import { VideoProcessor } from './videos/video.processor';
import { S3Service } from './AWS/s3/s3.service';
import { ConfigModule } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { videoCommentProviders } from './videos/video.comment.providers';
@Module({
  imports: [
    ConfigModule.forRoot(),
    DatabaseModule,
    UsersModule,
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '60d' },
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
      exclude: ['/api*'],
    }),
    VideosModule,

  ],
  controllers: [AppController,AuthenticationController],
  providers: [...userProviders, ...videoProviders, ...videoCommentProviders, AppService, AuthenticationService, JwtStrategy, UsersService,VideoProcessor, S3Service],
})
export class AppModule { }
