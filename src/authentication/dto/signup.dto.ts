import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty } from "class-validator";


export class SignupDto {

    @ApiProperty({type:String, description: 'user first name'})
    @IsNotEmpty()
    firstname: string;

    @ApiProperty({type:String, description: 'user last name'})
    @IsNotEmpty()
    lastname:string;


    @ApiProperty({type:String, description: 'user email'})
    @IsEmail()
    email:string;

    @ApiProperty({type:String, description: 'user password'})
    @IsNotEmpty()
    password:string;

    @ApiProperty({type:String, description: 'password confirmation'})
    @IsNotEmpty()
    passwordConfirmation:string;

}
