import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty } from "class-validator";
export class SigninDto {

    @ApiProperty({type:String, description: 'user first name'})
    @IsNotEmpty()
    email: string;


    @ApiProperty({type:String, description: 'user password'})
    @IsNotEmpty()
    password: string;
}
