import { Inject, Injectable } from '@nestjs/common';

import { SigninDto } from './dto/signin.dto';
import { SignupDto } from './dto/signup.dto';
import { JwtService } from '@nestjs/jwt';
import { User } from '../users/entities/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { jwtConstants } from './constants';
import { UsersService } from '../users/users.service';
import { v4 as uuiv4 } from "uuid";

@Injectable()
export class AuthenticationService {
  constructor(
    @Inject('USER_REPOSITORY')
    private userRepository: Repository<User>,
    private userService: UsersService,
    private jwtService: JwtService
  ) { }
  async hashPassword(password) {
    return await bcrypt.hash(password, jwtConstants.saltOrRounds);
  }
  async checkUserExists(email: string): Promise<boolean> {
    let userExists = false;
    let user = await this.userRepository.findOneBy({ email: email });

    if (user != null) {
      userExists = true;
    } else {
      userExists = false;
    }
    return userExists;
  }



  async signUp(signUpDto: SignupDto): Promise<any> {
    let user = new User();
    user.uuid = uuiv4();
    user.active = true;
    user.email = signUpDto.email;
    user.firstname = signUpDto.firstname;
    user.lastname = signUpDto.lastname;
    user.password = await this.hashPassword(signUpDto.password);
  
    let savedUser = await this.userRepository.save(user);
    
    return this.userService.buildUser(savedUser); 
  }

  async signIn(signinDto: SigninDto): Promise<any> {
    
    let user = await this.userRepository.findOneBy({email: signinDto.email, active:true});
    
    if(!user) { //user not found return error. Do not give any detail
      return;
    }

    

    let passwordCorrect = bcrypt.compareSync(signinDto.password, user.password)
    if(!passwordCorrect) { return; }

    let recudedUser = this. userService.buildUser(user);
    let token = this.jwtService.sign(recudedUser);

    return token;
  }


}
