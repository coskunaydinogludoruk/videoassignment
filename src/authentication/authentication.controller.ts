import { Body, Controller, HttpException, Post, UseFilters, UseInterceptors } from '@nestjs/common';
import { SignupDto } from './dto/signup.dto';
import { AuthenticationService } from './authentication.service';
import { SigninDto } from './dto/signin.dto';
//npmimport { TransformInterceptor } from '../transform.interceptor';
import { ApiOperation, ApiProperty, ApiResponse, ApiTags } from '@nestjs/swagger';
import { HttpExceptionFilter } from '../exception-filters/http-exception.filter';
import { HttpStatus, Res } from '@nestjs/common';
import { Response } from 'express';

@Controller('authentication')
@ApiTags('Authentication')
export class AuthenticationController {
    constructor(private authenticationService: AuthenticationService) {

    }

    @Post('sign-up')
    @ApiOperation({ summary: 'User Sign Up' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'User creation failed' })
    @ApiResponse({ status: 401, description: 'Unauthenticated' })
    @ApiResponse({ status: 201, description: 'user created' })
    //@UseFilters(HttpExceptionFilter)
    //@UseInterceptors(TransformInterceptor)
    async signUp(@Body() signUpDto: SignupDto, @Res() res: Response): Promise<any> {
        if (signUpDto.password !== signUpDto.passwordConfirmation) {
            throw new HttpException('passwords must be identical', HttpStatus.BAD_REQUEST);
        }

        if (await this.authenticationService.checkUserExists(signUpDto.email) == true) {
            throw new HttpException('user exists', HttpStatus.BAD_REQUEST)
        }

        let createdUser = await this.authenticationService.signUp(signUpDto);
        return  res.status(HttpStatus.CREATED).json({ success: true, data: createdUser, message: "signup success" });


    }

    @Post('sign-in')
    @ApiOperation({ summary: 'User Sign in' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })

    @ApiResponse({ status: 401, description: 'Unauthenticated. email or password is wrong' })
    @ApiResponse({ status: 200, description: 'Sign in successfully' })
    //@UseFilters(HttpExceptionFilter)
    //@UseInterceptors(TransformInterceptor)
    async signIn(@Body() singInDto: SigninDto, @Res() res: Response): Promise<any> {
       
        let token = await this.authenticationService.signIn(singInDto);
        if (!token) {
            throw new HttpException('Authentication Failed', HttpStatus.UNAUTHORIZED);
        }

        return res.status(HttpStatus.OK).json({success:true, data: {token:token}});

    }



}
