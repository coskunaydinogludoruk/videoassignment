import { OnQueueActive, Process, Processor } from '@nestjs/bull';
import { Inject } from '@nestjs/common';
import { Job } from 'bull';
import { S3Service } from 'src/AWS/s3/s3.service';
import { Repository } from 'typeorm';
import { Video } from './entities/video.entity';
import { UploadStatus } from './enums/upload-status.enum';

@Processor('video')
export class VideoProcessor {
    async transcode(job: Job<unknown>) {
        let progress = 0;
        for (let i = 0; i < 100; i++) {
            await this.uploadToS3(job.data);
            progress += 10;
            await job.progress(progress);
        }
        return {};
    }

    constructor(private s3Service: S3Service,
        @Inject('VIDEO_REPOSITORY')
        private videoRepository: Repository<Video>,) {

    }

    @OnQueueActive()
    onActive(job: Job) {
        console.log(
            `Processing job ${job.id} of type ${job.name} with data ${job.data.file}...`,
        );
    }

    @Process('video-file')
    async uploadToS3(data: any) {

        let videoId = data.data.id;
        this.videoRepository.update({id: videoId}, {uploadStartTime: new Date(), uploadStatus: UploadStatus.InProgress});


        let s3file = await this.s3Service.uploadFile(data.data.file);
        this.videoRepository.update({id: videoId}, {uploadCompletionTime: new Date(), uploadStatus: UploadStatus.Completed, s3Path: s3file.Location});
        


    }

}

