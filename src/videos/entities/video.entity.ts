import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { UploadStatus } from '../enums/upload-status.enum';

@Entity()
export class Video {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    uuid: string;

    @Column()
    originalFilename: string;

    @Column({default: ''})
    s3Path: string;


    @Column()
    description: string;

    @Column({ length: 255 })
    title: string;

    @Column()
    encoding:string;

    @Column()
    mimetype: string;

    @Column()
    uploadedBy: number;

    @Column({default: null})
    insertedTime: Date;

    @Column({default: null})
    uploadStartTime: Date;

    @Column({default: null})
    uploadCompletionTime: Date;

    @Column({ default: UploadStatus.NotStarted })
    uploadStatus: UploadStatus

}
