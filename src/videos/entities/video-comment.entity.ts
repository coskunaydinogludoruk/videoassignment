import { User } from 'src/users/entities/user.entity';
import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, OneToOne, JoinTable, ManyToMany } from 'typeorm';
import { UploadStatus } from '../enums/upload-status.enum';

@Entity()
export class VideoComment {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    uuid: string;

    @Column()
    videoId: number

    @Column()
    insertedBy: number;

    @Column()
    comment: string;

    @Column({default: null})
    insertedTime: Date;

    @Column({ default: false })
    deleted: boolean

}
