import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile, HttpException, HttpStatus } from '@nestjs/common';
import { VideosService } from './videos.service';
import { CreateVideoDto } from './dto/create-video.dto';
import { UpdateVideoDto } from './dto/update-video.dto';
import { JwtAuthGuard } from '../guards/auth.guard';
import { UseGuards, Request, Res } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { Response } from 'express';
import { CreateCommenDto } from './dto/create-comment.dto';

@Controller('videos')
@ApiTags('Video')
export class VideosController {
  constructor(private readonly videosService: VideosService) { }

  @Post()
  @ApiOperation({ summary: 'Create Video' })
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('file'))
  async create(@UploadedFile() file: Express.Multer.File, @Request() req, @Body() createVideoDto: CreateVideoDto, @Res() res: Response) {
    if (file === undefined) {
      throw new HttpException('Video file is missing', HttpStatus.BAD_REQUEST);
    }

    if (!this.videosService.checkVideoFileFormat(file)) {
      throw new HttpException('Unsupported file format', HttpStatus.BAD_REQUEST)
    }

    if (!this.videosService.checkVideoSize(file)) {

    }

    let createdVideo = await this.videosService.create(createVideoDto, file, req.user);
    this.videosService.uploadVideo(createdVideo.id, file);

    return res.status(HttpStatus.OK).json({ success: true, data: createdVideo });

  }

  @Get()
  @ApiOperation({ summary: 'Get videos' })
  @UseGuards(JwtAuthGuard)
  findAll() {
    return this.videosService.findAll();
  }

  @Get(':uuid')
  @ApiOperation({ summary: 'Get single video' })
  @UseGuards(JwtAuthGuard)
  findOne(@Param('uuid') uuid: string, @Res() res: Response) {
    return this.videosService.findOne(uuid).then(video => {
      if (!video) {
        throw new HttpException('video not found', HttpStatus.BAD_REQUEST);
      }
      return res.status(HttpStatus.OK).json({success: true, data: video});
    });
  }

  @Get(':uuid/comments')
  @ApiOperation({ summary: 'Get video comments' })
  @UseGuards(JwtAuthGuard)
  async getVideoComments(@Param('uuid') uuid: string, @Res() res: Response) {
    let comments = await this.videosService.getComments(uuid);
    return res.status(HttpStatus.OK).json({success: true, data: comments});

  }

  @Post(':uuid/comments')
  @ApiOperation({ summary: 'create video comment' })
  @UseGuards(JwtAuthGuard)
  async addVideoComment(@Param('uuid') uuid: string, 
                  @Request() req,
                  @Res() res: Response,
                  @Body() createCommentDto: CreateCommenDto) {
    let createdComment =  await this.videosService.addComment(uuid, req.user, createCommentDto);
    return res.status(HttpStatus.OK).json({success: true, data: createdComment});
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateVideoDto: UpdateVideoDto) {
    return this.videosService.update(+id, updateVideoDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.videosService.remove(+id);
  }
}


