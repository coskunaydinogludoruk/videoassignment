import { Connection } from 'typeorm';
import { VideoComment } from './entities/video-comment.entity';
import { Video } from './entities/video.entity';

export const videoCommentProviders = [
    {
      provide: 'VIDEO_COMMENT_REPOSITORY',
      useFactory: (connection: Connection) => connection.getRepository(VideoComment),
      inject: ['DATABASE_CONNECTION'],
    },
  ];