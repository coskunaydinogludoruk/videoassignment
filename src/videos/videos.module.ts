import { Module } from '@nestjs/common';
import { VideosService } from './videos.service';
import { VideosController } from './videos.controller';
import { videoProviders } from './video.providers';
import { DatabaseModule } from '../database/database.module';
import { BullModule } from '@nestjs/bull';
import { S3Service } from 'src/AWS/s3/s3.service';
import { videoCommentProviders } from './video.comment.providers';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    DatabaseModule,
    BullModule.forRoot({
      redis: {
        host: process.env.REDIS_SERVER,
        port: <number><unknown>process.env.REDIS_PORT
      },
    }),
    BullModule.registerQueue({
      name: 'video',
    })],
  controllers: [VideosController],
  providers: [...videoProviders, ...videoCommentProviders, VideosService, S3Service ]
})
export class VideosModule {}
