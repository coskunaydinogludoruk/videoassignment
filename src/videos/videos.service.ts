import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { getManager, Repository } from 'typeorm';
import { CreateVideoDto } from './dto/create-video.dto';
import { UpdateVideoDto } from './dto/update-video.dto';
import { Video } from './entities/video.entity';
import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import { v4 as uuiv4 } from "uuid";
import { CreateCommenDto } from './dto/create-comment.dto';
import { VideoComment } from './entities/video-comment.entity';


@Injectable()
export class VideosService {


  constructor(@Inject('VIDEO_REPOSITORY')
  private videoRepository: Repository<Video>,
    @Inject('VIDEO_COMMENT_REPOSITORY')
    private videoCommentRepository: Repository<VideoComment>,
    @InjectQueue('video') private videoQueue: Queue) {

  }

  uploadVideo(videoId: number, videoFile) {
    
    //write video to queue
    //Queue processor will uplad to S3 bucket
    this.videoQueue.add('video-file', { id: videoId, file: videoFile }).then(
      (task) => { },
      (error) => { console.log("queue addition error", error) }
    );
  }

  checkVideoFileFormat(videoFile: Express.Multer.File) {
    let videoMimes = [
      'video/x-flv',
      'video/mp4',
      'application/x-mpegURL',
      'video/MP2T',
      'video/3gpp',
      'video/quicktime',
      'video/x-msvideo',
      'video/x-ms-wmv'
    ]

    return videoMimes.includes(videoFile.mimetype);
  }

  checkVideoSize(videoFile: Express.Multer.File) {
    return videoFile.size <= <number><unknown>process.env.MAX_FILESIZE
  }


  async create(createVideoDto: CreateVideoDto, file, user: any) {
    let video = new Video();
    video.description = createVideoDto.description;
    video.title = createVideoDto.title;
    video.uploadedBy = user.id;
    video.originalFilename = file.originalname;
    video.encoding = file.encoding;
    video.mimetype = file.mimetype;
    video.uuid = uuiv4();
    video.insertedTime = new Date();


    let savedVideo = await this.videoRepository.save(video);

    return savedVideo;
  }

  findAll() {
    return this.videoRepository.find();
  }

  findOne(uuid: string) {
    return this.videoRepository.findOneBy({ uuid: uuid });
  }

  update(id: number, updateVideoDto: UpdateVideoDto) {
    return `This action updates a #${id} video`;
  }

  remove(id: number) {
    return `This action removes a #${id} video`;
  }

  async getComments(uuid: string) {
    let video = await this.findOne(uuid);
    if (!video) {
      throw new HttpException('Invalid video', HttpStatus.BAD_REQUEST);
    }
    const entityManager = getManager();
    let comments = await entityManager.query(`SELECT 
                                               vc.uuid, 
                                               vc.comment,
                                               u.firstname,
                                               u.lastname
                                               FROM video_comment  vc INNER JOIN user u  ON vc.insertedBy = u.id
                                               WHERE vc.videoId = ? and vc.deleted=0`
      , [video.id]);

    return comments;

  }
  async addComment(uuid: string, user, createCommentDto: CreateCommenDto) {
    let video = await this.findOne(uuid);
    if (!video) {
      throw new HttpException('video does not exists', HttpStatus.BAD_REQUEST);
    }
    let videoComment = new VideoComment();
    videoComment.comment = createCommentDto.comment;
    videoComment.videoId = video.id;
    videoComment.insertedBy = user.id;
    videoComment.insertedTime = new Date();
    videoComment.uuid = uuiv4();
    let savedComment = await this.videoCommentRepository.save(videoComment);
    return savedComment;
  }
}
