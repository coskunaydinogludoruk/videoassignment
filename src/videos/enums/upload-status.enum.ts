export enum UploadStatus {
    NotStarted = 1,
    InProgress = 2,
    Completed = 3
}