import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class CreateVideoDto {
    @ApiProperty({type:String, description: 'video description'})
    @IsNotEmpty()
    description: string;

    @ApiProperty({type:String, description: 'video title'})
    @IsNotEmpty()
    title: string;



}
