import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, MaxLength } from "class-validator";
export class CreateCommenDto {
    @ApiProperty({type:String, description: 'video comment enteed by user'})
    @IsNotEmpty()
    @MaxLength(2500)
    comment: string;
}