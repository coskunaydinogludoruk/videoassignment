import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    
    @Column()
    uuid: string;

    @Column({ length: 255 })
    firstname: string;

    @Column({ length: 255 })
    lastname: string;

    @Column({ length: 255 })
    email: string;

    @Column('text')
    password: string;

    @Column()
    active: boolean;


}
