import { Injectable } from '@nestjs/common';
import * as AWS from "aws-sdk";
import * as mime from "mime-types";
import { v4 as uuiv4 } from "uuid";
@Injectable()
export class S3Service {
    
    AWS_S3_BUCKET =  process.env.AWS_S3_BUCKET;
    s3 = new AWS.S3
        ({
            accessKeyId: process.env.AWS_S3_ACCESS_KEY,
            secretAccessKey:  process.env.AWS_S3_KEY_SECRET,
        });

    async uploadFile(file){

        let extension = mime.extension(file.mimetype);
        let fileName = uuiv4() + "." + extension;

        return await this.s3Upload(file.buffer, this.AWS_S3_BUCKET, fileName, file.mimetype);
    }

    private async s3Upload(fileBuffer, bucket, name, mimetype) {
        const params =
        {
            Bucket: bucket,
            Key: String(name),
            Body: Buffer.from(fileBuffer),
            //ACL: "public-read",
            ContentType: mimetype,
            ContentDisposition: "inline",
            CreateBucketConfiguration:
            {
                LocationConstraint: process.env.AWS_REGION
            }
        };


        try {
            let s3Response = await this.s3.upload(params).promise();

            return s3Response;
        }
        catch (e) {
            console.log(e);
        }
    }
}