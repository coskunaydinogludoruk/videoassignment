import { createConnection } from 'typeorm';

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async () => await createConnection({
      type: 'mysql',
      host:  process.env.MYSQL_HOST,
      port:  <number><unknown>process.env.MYSQL_PORT,
      username:  process.env.MYSQL_USER,
      password: process.env.MYSQL_USER_PASS,
      database: process.env.MYSQL_DB_NAME,
      entities: [
          __dirname + '/../**/*.entity{.ts,.js}',
      ],
      synchronize: true,
    }),
  },
];