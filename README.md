## Description

Video upload assignment.

## Installation

Local installation
- install MySql
- install Redis Server
- Create public S3 bucket on AWS.
- Create programmatic S3 user on AWS. Get the S3 Credentials
- Pull the repository
- Create .env file. Fill the .env configuration

```bash
$ npm install
```
For development mode
```bash
$ npm run start:dev
```

## Running the app
To run docker image
```bash
docker-compose build
```
```bash
docker-compose up
```

To run locally
```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
Application runs on port 9001
localhost:9001



## Documentation

Swagger Documentation
http://localhost:9001/api

Postman 
https://www.postman.com/fleetmanagementibb/workspace/videoassignment/overview

User signs up with email, password, first name, last name
User signs in with email and password
After successful login user uploads video by sending POST request to /videos endpoint (details are in POSTMAN and Swagger)
When video is uploaded video metadata (title, name, filename etc.) is saved to mysql database. Videofile and its record id is added to queue. 

Queue consumer (videos/video.processor.ts) reads the queue and uploads the video to S3 Bucket. Updates the video record when upload is completed.




